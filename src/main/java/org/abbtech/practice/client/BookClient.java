package org.abbtech.practice.client;

import org.hibernate.validator.constraints.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.awt.print.Book;

@FeignClient(name = "book-service", url = "http://localhost:8080")
public interface BookClient {
    @GetMapping("/books/{id}")
    Book getBookById(@PathVariable UUID id);
}