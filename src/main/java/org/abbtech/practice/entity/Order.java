package org.abbtech.practice.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Setter
@Getter
public class Order {
    @Id
    @GeneratedValue
    private UUID id;
    private UUID bookId;
    private int quantity;
    private UUID userId;

    public void setUserId(java.util.UUID uuid) {
    }
}
