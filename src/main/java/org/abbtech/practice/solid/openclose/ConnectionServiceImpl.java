package org.abbtech.practice.solid.openclose;

public class ConnectionServiceImpl {
    private final ConnectionService connectionService;

    public ConnectionServiceImpl(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public void getConnection() {
        this.connectionService.getConnection();
    }
}
