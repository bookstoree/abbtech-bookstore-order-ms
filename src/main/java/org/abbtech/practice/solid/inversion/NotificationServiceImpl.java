package org.abbtech.practice.solid.inversion;

public class NotificationServiceImpl implements NotificationService {

    private final MessageSender messageSender;

    public NotificationServiceImpl(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    @Override
    public void sendNotification() {
        messageSender.sendMessage();
    }
//    private final EmailMessageSender emailMessageSender;
//    private final SMSMessageSender smsMessageSender;
//
//    public NotificationService() {
//        this.emailMessageSender = new EmailMessageSender();
//        this.smsMessageSender = new SMSMessageSender();
//    }
//
//    public void sendEmailMessageSender() {
//        emailMessageSender.sendMessage();
//    }
//
//    public void sendESmsMessageSender() {
//        smsMessageSender.sendMessage();
//    }


}
