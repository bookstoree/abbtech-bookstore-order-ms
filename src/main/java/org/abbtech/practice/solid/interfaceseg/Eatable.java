package org.abbtech.practice.solid.interfaceseg;

public interface Eatable {
    void eat();
}
