package org.abbtech.practice.controller;

import org.abbtech.practice.client.BookClient;
import org.abbtech.practice.entity.Order;
import org.abbtech.practice.exception.ResourceNotFoundException;
import org.abbtech.practice.kafka.KafkaTemplate;
import org.abbtech.practice.repostory.OrderRepository;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.util.UUID;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderRepository orderRepository;
    private final BookClient bookClient;
    private final KafkaTemplate<String, String> kafkaTemplate;

    public OrderController(OrderRepository orderRepository, BookClient bookClient, KafkaTemplate<String, String> kafkaTemplate) {
        this.orderRepository = orderRepository;
        this.bookClient = bookClient;
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostMapping
    public Order createOrder(@RequestBody Order order, @RequestHeader("X-USER-ID") String userId, @RequestHeader("X-USER-EMAIL") String userEmail) throws ResourceNotFoundException {
        Book book = bookClient.getBookById(order.getBookId());
        if (book == null) {
            throw new ResourceNotFoundException("Book not found");
        }
        order.setUserId(UUID.fromString(userId));
        Order savedOrder = orderRepository.save(order);
        kafkaTemplate.send("order-topic", "Order Created: " + savedOrder.getId());
        return savedOrder;
    }

    @PostMapping("/confirm")
    public void confirmOrder(@RequestBody UUID orderId) {
        kafkaTemplate.send("order-confirm-topic", "Order Confirmed: " + orderId);
    }

    @GetMapping("/{id}")
    public Order getOrderById(@PathVariable UUID id) throws ResourceNotFoundException {
        return (Order) orderRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Order not found"));
    }
}
