package org.abbtech.practice.exception;

import lombok.Getter;
import org.abbtech.practice.exception.enums.GeneralExceptionEnum;

@Getter
public class GeneralTechException extends RuntimeException {
    private final GeneralExceptionEnum generalExceptionEnum;

    public GeneralTechException(GeneralExceptionEnum generalExceptionEnum) {
        this.generalExceptionEnum = generalExceptionEnum;
    }

    public GeneralTechException(GeneralExceptionEnum generalExceptionEnum, Throwable throwable) {
        super(generalExceptionEnum.toString(), throwable);
        this.generalExceptionEnum = generalExceptionEnum;
    }
}
